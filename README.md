# OAuth and social login

This project demostrate how to OAuth and social login work on Authorization code grant type to access google drive api 

### Special npm command 
```
npm install
```

### Run application for execute below command in terminal/cmd
```
node index
```

#### Application steps
 1. copy showing url in terminal and paste it in brower and brows
 2. provide your google credential for login if you are not yet logged your google account, other wise follow google provide steps
 3. Then you can see token in your browser. Copy it and paste it in requested area in your terminal.
 4. Finally you can see your latest 10 files name in your terminal.